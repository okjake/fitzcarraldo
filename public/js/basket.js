var Basket = {
	clear: function() {
		var _basket = localStorage.getItem("basket");
		if (_basket) {
			basket = JSON.parse(_basket);
			basket.contents = [];
			localStorage.setItem("basket", JSON.stringify(basket));
		}
	},

	getCount: function(next) {
		var _basket = localStorage.getItem("basket");
		if (_basket) {
			basket = JSON.parse(_basket);
			if (!basket) return next(0);
			var count = basket.contents ? basket.contents.length : 0;
			next(count);
		} else {
			next(0);
		}
	},

	getOrderString: function() {
		var _basket = localStorage.getItem("basket");
		if (_basket) basket = JSON.parse(_basket);

		var shipping = localStorage.getItem("shipping");
		if (!shipping) shipping = "uk";

		var promoCode = localStorage.getItem("promoCode");
		if (!promoCode) promoCode = "";

		var additional = localStorage.getItem("additional");
		if (!additional) additional = "";

		if (!basket.contents.length) return next();

		return JSON.stringify({
			promoCode: promoCode,
			shipping: shipping,
			contents: basket.contents,
			additional: additional
		});
	},

	setPromoCode: function(code, next) {
		localStorage.setItem("promoCode", code);
		next();
	},

	getPromoCode: function() {
		return localStorage.getItem("promoCode");
	},

	setShipping: function(shipping, next) {
		localStorage.setItem("shipping", shipping);
		next();
	},

	getShipping: function() {
		return localStorage.getItem("shipping");
	},

	setAdditional: function(additional, next) {
		localStorage.setItem("additional", additional);
		if (next) next();
	},

	getAdditional: function() {
		return localStorage.getItem("additional");
	},

	addItem: function(type, id, qty, next, notes) {
		var item = {
			type: type,
			id: id,
			qty: qty,
			notes: notes
		};

		var basket = { contents: [] },
			_basket = localStorage.getItem("basket"),
			resolved = false;

		if (_basket) basket = JSON.parse(_basket);

		if (basket && basket.contents) {
			for (var i = 0; i < basket.contents.length; i++) {
				if (basket.contents[i].id == id) {
					var quantity = (basket.contents[i].qty + qty);
					var min=1;
					var max=10;
					basket.contents[i].qty = (quantity > min) ? ((quantity < max) ? quantity : max) : min;
					resolved = true;
				}
			}
		} else basket = { contents: [] };

		if (!resolved) basket.contents.push(item);
		_basket = JSON.stringify(basket);

		localStorage.setItem("basket", _basket);

		next();
	},

	removeItem: function(id, next) {
		var _basket = localStorage.getItem("basket");
		if (_basket) basket = JSON.parse(_basket);

		for (var i = 0; i < basket.contents.length; i++) {
			if (basket.contents[i].id == id) {
				var index = i;
				break;
			}
		}

		basket.contents.splice(index, 1);
		_basket = JSON.stringify(basket);

		localStorage.setItem("basket", _basket);
		next();
	},

	updateItem: function(id, qty, next) {
		var _basket = localStorage.getItem("basket");
		if (_basket) basket = JSON.parse(_basket);

		for (var i = 0; i < basket.contents.length; i++) {
			if (basket.contents[i].id == id) {
				basket.contents[i].qty = parseInt(qty);
			}
		}

		_basket = JSON.stringify(basket);
		localStorage.setItem("basket", _basket);
		next();
	},

	populate: function(next) {
		var _basket = localStorage.getItem("basket");
		if (_basket) {
			basket = JSON.parse(_basket);
		} else {
			$(".checkout").fadeOut(function() {
				$("#checkout-empty").fadeIn();
			});
			return false;
		}

		var shipping = localStorage.getItem("shipping");
		if (!shipping) shipping = "uk";

		var promoCode = localStorage.getItem("promoCode");
		if (!promoCode) promoCode = "";

		if (!basket.contents.length) return next();

		$.post("/api/basket", {
			shipping: shipping,
			promoCode: promoCode,
			contents: basket.contents
		}).done(next);
	}
};

var BasketUI = {
	updateCount: function() {
		Basket.getCount(function(count) {
			$(".basket__container--number").text(count);
		});
	},

	formatCurrency: function(amount) {
		return "£" + Number(amount).toFixed(2);
	},

	formatDiscount(item) {
		if (!item.discount)
			return BasketUI.formatCurrency(item.sum.excludingShipping);
		else
			return `<s>${BasketUI.formatCurrency(
				item.sum.excludingShipping
			)}</s> ${BasketUI.formatCurrency(
				item.sum.excludingShipping - item.discount
			)}`;
	},

	renderRow: function(data, showPrice) {
		var markup = $("#row-template").html();
		markup = markup.replace(/-id-/g, data.id);
		markup = markup.replace(/-url-/g, data.url);
		markup = markup.replace(/-title-/g, data.title);
		markup = markup.replace(/-author-/g, data.author);
		if (data.format) {
			markup = markup.replace(
				/-format-/g,
				data.format.charAt(0).toUpperCase() + data.format.substring(1)
			);
		} else {
			markup = markup.replace(/-format-/g, "Bundle");
		}

		markup = markup.replace(
			/-unit-price-/g,
			BasketUI.formatCurrency(data.individual.excludingShipping)
		);

		markup = markup.replace(/-sum-price-/g, BasketUI.formatDiscount(data));

		// gross hax
		markup = markup.replace(
			'<option value="' + data.qty + '">',
			'<option value="' + data.qty + '" selected>'
		);
		$("#basket-contents").append(markup);
	},

	populate: function() {
		Basket.populate(function(data) {
			if (data) {
				var description = "";
				$("#basket-contents, #total").html("");
				$("#shipping-select").val(Basket.getShipping());
				$(".checkout__shipping--promocode-input").val(Basket.getPromoCode());
				$("#shipping-cost").html(BasketUI.formatCurrency(data.totals.shipping));
				var discount = data.checkoutMessage
					? `Promotion: <span>${data.checkoutMessage}</span>`
					: data.excludingShippingDiscount
					? `Discount: <span>${BasketUI.formatCurrency(
							data.excludingShippingDiscount
					  )}</span>`
					: "";
				$("#discount").html(discount);
				for (var i = 0; i < data.items.length; i++) {
					BasketUI.renderRow(data.items[i], !data.checkoutMessage);
				}

				$("#pay").attr("data-desc", data.description);
				$("#pay").attr("data-amt", (data.totals.total * 100).toFixed(0));

				$("#total").html(BasketUI.formatCurrency(data.totals.total));
				$(".checkout").fadeIn();
			} else {
				$(".checkout").fadeOut(function() {
					$("#checkout-empty").fadeIn();
				});
			}
			BasketUI.updateCount();
		});
	}
};
