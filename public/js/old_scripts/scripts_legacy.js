/**main app js**/
/*! nanoScrollerJS - v0.8.0 - (c) 2014 James Florentino; Licensed MIT */

!function(a,b,c){"use strict";var d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F;x={paneClass:"nano-pane",sliderClass:"nano-slider",contentClass:"nano-content",iOSNativeScrolling:!1,preventPageScrolling:!1,disableResize:!1,alwaysVisible:!1,flashDelay:1500,sliderMinHeight:20,sliderMaxHeight:null,documentContext:null,windowContext:null},s="scrollbar",r="scroll",k="mousedown",l="mousemove",n="mousewheel",m="mouseup",q="resize",h="drag",u="up",p="panedown",f="DOMMouseScroll",g="down",v="wheel",i="keydown",j="keyup",t="touchmove",d="Microsoft Internet Explorer"===b.navigator.appName&&/msie 7./i.test(b.navigator.appVersion)&&b.ActiveXObject,e=null,B=b.requestAnimationFrame,w=b.cancelAnimationFrame,D=c.createElement("div").style,F=function(){var a,b,c,d,e,f;for(d=["t","webkitT","MozT","msT","OT"],a=e=0,f=d.length;f>e;a=++e)if(c=d[a],b=d[a]+"ransform",b in D)return d[a].substr(0,d[a].length-1);return!1}(),E=function(a){return F===!1?!1:""===F?a:F+a.charAt(0).toUpperCase()+a.substr(1)},C=E("transform"),z=C!==!1,y=function(){var a,b,d;return a=c.createElement("div"),b=a.style,b.position="absolute",b.width="100px",b.height="100px",b.overflow=r,b.top="-9999px",c.body.appendChild(a),d=a.offsetWidth-a.clientWidth,c.body.removeChild(a),d},A=function(){var a,c,d;return c=b.navigator.userAgent,(a=/(?=.+Mac OS X)(?=.+Firefox)/.test(c))?(d=/Firefox\/\d{2}\./.exec(c),d&&(d=d[0].replace(/\D+/g,"")),a&&+d>23):!1},o=function(){function i(d,f){this.el=d,this.options=f,e||(e=y()),this.$el=a(this.el),this.doc=a(this.options.documentContext||c),this.win=a(this.options.windowContext||b),this.$content=this.$el.children("."+f.contentClass),this.$content.attr("tabindex",this.options.tabIndex||0),this.content=this.$content[0],this.previousPosition=0,this.options.iOSNativeScrolling&&null!=this.el.style.WebkitOverflowScrolling?this.nativeScrolling():this.generate(),this.createEvents(),this.addEvents(),this.reset()}return i.prototype.preventScrolling=function(a,b){if(this.isActive)if(a.type===f)(b===g&&a.originalEvent.detail>0||b===u&&a.originalEvent.detail<0)&&a.preventDefault();else if(a.type===n){if(!a.originalEvent||!a.originalEvent.wheelDelta)return;(b===g&&a.originalEvent.wheelDelta<0||b===u&&a.originalEvent.wheelDelta>0)&&a.preventDefault()}},i.prototype.nativeScrolling=function(){this.$content.css({WebkitOverflowScrolling:"touch"}),this.iOSNativeScrolling=!0,this.isActive=!0},i.prototype.updateScrollValues=function(){var a,b;a=this.content,this.maxScrollTop=a.scrollHeight-a.clientHeight,this.prevScrollTop=this.contentScrollTop||0,this.contentScrollTop=a.scrollTop,b=this.contentScrollTop>this.previousPosition?"down":this.contentScrollTop<this.previousPosition?"up":"same",this.previousPosition=this.contentScrollTop,"same"!==b&&this.$el.trigger("update",{position:this.contentScrollTop,maximum:this.maxScrollTop,direction:b}),this.iOSNativeScrolling||(this.maxSliderTop=this.paneHeight-this.sliderHeight,this.sliderTop=0===this.maxScrollTop?0:this.contentScrollTop*this.maxSliderTop/this.maxScrollTop)},i.prototype.setOnScrollStyles=function(){var a;z?(a={},a[C]="translate(0, "+this.sliderTop+"px)"):a={top:this.sliderTop},B?this.scrollRAF||(this.scrollRAF=B(function(b){return function(){b.scrollRAF=null,b.slider.css(a)}}(this))):this.slider.css(a)},i.prototype.createEvents=function(){this.events={down:function(a){return function(b){return a.isBeingDragged=!0,a.offsetY=b.pageY-a.slider.offset().top,a.pane.addClass("active"),a.doc.bind(l,a.events[h]).bind(m,a.events[u]),!1}}(this),drag:function(a){return function(b){return a.sliderY=b.pageY-a.$el.offset().top-a.offsetY,a.scroll(),a.contentScrollTop>=a.maxScrollTop&&a.prevScrollTop!==a.maxScrollTop?a.$el.trigger("scrollend"):0===a.contentScrollTop&&0!==a.prevScrollTop&&a.$el.trigger("scrolltop"),!1}}(this),up:function(a){return function(){return a.isBeingDragged=!1,a.pane.removeClass("active"),a.doc.unbind(l,a.events[h]).unbind(m,a.events[u]),!1}}(this),resize:function(a){return function(){a.reset()}}(this),panedown:function(a){return function(b){return a.sliderY=(b.offsetY||b.originalEvent.layerY)-.5*a.sliderHeight,a.scroll(),a.events.down(b),!1}}(this),scroll:function(a){return function(b){a.updateScrollValues(),a.isBeingDragged||(a.iOSNativeScrolling||(a.sliderY=a.sliderTop,a.setOnScrollStyles()),null!=b&&(a.contentScrollTop>=a.maxScrollTop?(a.options.preventPageScrolling&&a.preventScrolling(b,g),a.prevScrollTop!==a.maxScrollTop&&a.$el.trigger("scrollend")):0===a.contentScrollTop&&(a.options.preventPageScrolling&&a.preventScrolling(b,u),0!==a.prevScrollTop&&a.$el.trigger("scrolltop"))))}}(this),wheel:function(a){return function(b){var c;if(null!=b)return c=b.delta||b.wheelDelta||b.originalEvent&&b.originalEvent.wheelDelta||-b.detail||b.originalEvent&&-b.originalEvent.detail,c&&(a.sliderY+=-c/3),a.scroll(),!1}}(this)}},i.prototype.addEvents=function(){var a;this.removeEvents(),a=this.events,this.options.disableResize||this.win.bind(q,a[q]),this.iOSNativeScrolling||(this.slider.bind(k,a[g]),this.pane.bind(k,a[p]).bind(""+n+" "+f,a[v])),this.$content.bind(""+r+" "+n+" "+f+" "+t,a[r])},i.prototype.removeEvents=function(){var a;a=this.events,this.win.unbind(q,a[q]),this.iOSNativeScrolling||(this.slider.unbind(),this.pane.unbind()),this.$content.unbind(""+r+" "+n+" "+f+" "+t,a[r])},i.prototype.generate=function(){var a,c,d,f,g,h,i;return f=this.options,h=f.paneClass,i=f.sliderClass,a=f.contentClass,(g=this.$el.children("."+h)).length||g.children("."+i).length||this.$el.append('<div class="'+h+'"><div class="'+i+'" /></div>'),this.pane=this.$el.children("."+h),this.slider=this.pane.find("."+i),0===e&&A()?(d=b.getComputedStyle(this.content,null).getPropertyValue("padding-right").replace(/\D+/g,""),c={right:-14,paddingRight:+d+14}):e&&(c={right:-e},this.$el.addClass("has-scrollbar")),null!=c&&this.$content.css(c),this},i.prototype.restore=function(){this.stopped=!1,this.iOSNativeScrolling||this.pane.show(),this.addEvents()},i.prototype.reset=function(){var a,b,c,f,g,h,i,j,k,l,m,n;return this.iOSNativeScrolling?void(this.contentHeight=this.content.scrollHeight):(this.$el.find("."+this.options.paneClass).length||this.generate().stop(),this.stopped&&this.restore(),a=this.content,f=a.style,g=f.overflowY,d&&this.$content.css({height:this.$content.height()}),b=a.scrollHeight+e,l=parseInt(this.$el.css("max-height"),10),l>0&&(this.$el.height(""),this.$el.height(a.scrollHeight>l?l:a.scrollHeight)),i=this.pane.outerHeight(!1),k=parseInt(this.pane.css("top"),10),h=parseInt(this.pane.css("bottom"),10),j=i+k+h,n=Math.round(j/b*j),n<this.options.sliderMinHeight?n=this.options.sliderMinHeight:null!=this.options.sliderMaxHeight&&n>this.options.sliderMaxHeight&&(n=this.options.sliderMaxHeight),g===r&&f.overflowX!==r&&(n+=e),this.maxSliderTop=j-n,this.contentHeight=b,this.paneHeight=i,this.paneOuterHeight=j,this.sliderHeight=n,this.slider.height(n),this.events.scroll(),this.pane.show(),this.isActive=!0,a.scrollHeight===a.clientHeight||this.pane.outerHeight(!0)>=a.scrollHeight&&g!==r?(this.pane.hide(),this.isActive=!1):this.el.clientHeight===a.scrollHeight&&g===r?this.slider.hide():this.slider.show(),this.pane.css({opacity:this.options.alwaysVisible?1:"",visibility:this.options.alwaysVisible?"visible":""}),c=this.$content.css("position"),("static"===c||"relative"===c)&&(m=parseInt(this.$content.css("right"),10),m&&this.$content.css({right:"",marginRight:m})),this)},i.prototype.scroll=function(){return this.isActive?(this.sliderY=Math.max(0,this.sliderY),this.sliderY=Math.min(this.maxSliderTop,this.sliderY),this.$content.scrollTop((this.paneHeight-this.contentHeight+e)*this.sliderY/this.maxSliderTop*-1),this.iOSNativeScrolling||(this.updateScrollValues(),this.setOnScrollStyles()),this):void 0},i.prototype.scrollBottom=function(a){return this.isActive?(this.$content.scrollTop(this.contentHeight-this.$content.height()-a).trigger(n),this.stop().restore(),this):void 0},i.prototype.scrollTop=function(a){return this.isActive?(this.$content.scrollTop(+a).trigger(n),this.stop().restore(),this):void 0},i.prototype.scrollTo=function(a){return this.isActive?(this.scrollTop(this.$el.find(a).get(0).offsetTop),this):void 0},i.prototype.stop=function(){return w&&this.scrollRAF&&(w(this.scrollRAF),this.scrollRAF=null),this.stopped=!0,this.removeEvents(),this.iOSNativeScrolling||this.pane.hide(),this},i.prototype.destroy=function(){return this.stopped||this.stop(),!this.iOSNativeScrolling&&this.pane.length&&this.pane.remove(),d&&this.$content.height(""),this.$content.removeAttr("tabindex"),this.$el.hasClass("has-scrollbar")&&(this.$el.removeClass("has-scrollbar"),this.$content.css({right:""})),this},i.prototype.flash=function(){return!this.iOSNativeScrolling&&this.isActive?(this.reset(),this.pane.addClass("flashed"),setTimeout(function(a){return function(){a.pane.removeClass("flashed")}}(this),this.options.flashDelay),this):void 0},i}(),a.fn.nanoScroller=function(b){return this.each(function(){var c,d;if((d=this.nanoscroller)||(c=a.extend({},x,b),this.nanoscroller=d=new o(this,c)),b&&"object"==typeof b){if(a.extend(d.options,b),null!=b.scrollBottom)return d.scrollBottom(b.scrollBottom);if(null!=b.scrollTop)return d.scrollTop(b.scrollTop);if(b.scrollTo)return d.scrollTo(b.scrollTo);if("bottom"===b.scroll)return d.scrollBottom(0);if("top"===b.scroll)return d.scrollTop(0);if(b.scroll&&b.scroll instanceof a)return d.scrollTo(b.scroll);if(b.stop)return d.stop();if(b.destroy)return d.destroy();if(b.flash)return d.flash()}return d.reset()})},a.fn.nanoScroller.Constructor=o}(jQuery,window,document);

$(document).ready(function() {

	// Delegate .transition() calls to .animate()
	// if the browser can't do CSS transitions.
	if (!$.support.transition)
	  $.fn.transition = $.fn.animate;

	  $('#twitter').sharrre({
	      share: {
	        twitter: true
	      },
	      template: '<a class="box" href="#"><div class="count" href="#">{total}</div><div class="share"><span></span>twitter</div></a>',
	      enableHover: false,
	      enableTracking: true,
	      buttons: { twitter: {via: 'FitzcarraldoEds'}},
	      click: function(api, options){
	          api.simulateClick();
	         api.openPopup('twitter');
	      }
	  });

	  $('#facebook').sharrre({
	      share: {
	          facebook: true
	      },
	      template: '<a class="box" href="#"><div class="count" href="#">{total}</div><div class="share"><span></span>facebook</div></a>',
	      enableHover: false,
	      enableTracking: true,
	      click: function(api, options){
	          api.simulateClick();
	          api.openPopup('facebook');
	      }
	  });

	  $('li.last').hover(function(e){
		    if (e.target === this) {
			      $(this).find('#share').finish().transition({ y: '-30px' }, 250, 'snap');
			      $('#share_btns li:first-child').finish().delay(100).transition({ y: '-27px' }, 500, 'snap');
			      $('#share_btns li:last-child').finish().delay(300).transition({ y: '-27px' }, 500, 'snap');
	      }
	  }, function(e){
		    if (e.target === this) {
			      $('#share').finish().delay(200).transition({ y: '0px' }, 500, 'snap');
			      $('#share_btns li:first-child').finish().delay(100).transition({ y: '0px' }, 250, 'snap');
			      $('#share_btns li:last-child').finish().transition({ y: '0px' }, 250, 'snap');
		    }
	  });


	  //subscription check boxes
	  $('#about_text').on('click', '.button', function(){
	  	var el = $(this).attr('data-substype');

	  	$('#firstModal').find('input[type=checkbox]').each(function(i){
	  		if($(this).attr('id') == el) {
		  		$(this).prop("checked", true);
		  	} else {
		  		$(this).attr('disabled', 'disabled');
		  	}
	  	});


	  });

	  $(document).on('close.fndtn.reveal', '[data-reveal]', function () {
		  var modal = $(this);
		  modal.find('input[type=checkbox]').prop("checked", false).removeAttr('disabled');
	  });









 var current_width = $(window).width();
 if (current_width > 940) {

		 var controller = new ScrollMagic.Controller();

	    var headerHeight = $('#menu_hook').find('.fixed').outerHeight(),
	   	  footerHeight = $('#footer__container').outerHeight();
	   	  breadcrumbHeight = 0,
	   	  bookHeight = 0;


	    if ($('#breadcrumb').length) {
	   	 breadcrumbHeight = $('#breadcrumb').outerHeight();
	    }

	    if ($('#book_pic').length) {
	   	 bookHeight = $('#book_pic').outerHeight();
	    }


	   var offsetval = 25,
	   	duration_offset = breadcrumbHeight + headerHeight + bookHeight + footerHeight + 120;
	   	duration_val = $(document).height() - duration_offset;


	   var nav = new ScrollMagic.Scene({triggerHook: "0", duration: duration_val})
	   				.offset(30)
	   				.addTo(controller);

	   var exitTrigger = new ScrollMagic.Scene({triggerHook: "0", duration: 20})
	   				.addTo(controller);


	   var $fitz = $('#fitzBrand');
	   var $fitz_a = $fitz.find('.mark_anchor');
	   var $fitz_mark = $fitz.find('.mark');


	   nav.on("enter", function (e) {

	   	if(e.scrollDirection == "FORWARD") {

	   		$fitz_a.stop().velocity({
	   			marginTop: '20px',
	   			width: '230px'
	   		}, 200);

	   	  $('.right > li').stop().velocity({
	   		paddingTop: '18px'
	   	  }, 200);

	   	  $fitz_mark.stop().velocity({
	   		marginTop: '14px',
	   		width: '33px'
	   	  }, 200);

	   	  $('#menu_hook').find('.fixed').addClass('shadow');


	     }


	   });



	    exitTrigger.on("enter", function (e) {

	   	 if(e.scrollDirection == "REVERSE") {

	   		 $fitz_a.stop().velocity({
	   			marginTop: '38px',
	   			width: '300px'
	   		 }, 200);

	   		  $('.right > li').stop().velocity({
	   			paddingTop: '40px'
	   		 }, 200);

	   		 $fitz_mark.stop().velocity({
	   			marginTop: '30px',
	   			width: '40px'
	   		 }, 200);

	   		  $('#menu_hook').find('.fixed').removeClass('shadow');

	   	}

	   });


	   if($('#book_pic').length) {

	   	var bookscene = new ScrollMagic.Scene({triggerHook: "0", duration: duration_val})
	   					.setPin("#book_pic", {
	   						  pushFollowers: false
	   					 })
	   					.offset(80)
	   					.addTo(controller);

	   }


}


/**UNCOMMENT FOR OLD CAROUSEL**/
/**homepage carousel re-size issue**/
/*function resizeLoad(){

    var current_width = $(window).width();

    $('#cycle_covers').find('img').eq(1).hide();

	$('#cycle_covers').find('img').first().load(function() {

	  $('#cycle_covers').find('img').eq(1).show();
	  var img_h = $(this).height();

	  $('#fitzWrapper').children().css('height', img_h);
	  $('#cycle_covers').cycle({
			timeout : 5000
	   });

	});

}*/



/*function resizeHome(){
	var current_width = $(window).width();

	var imgHeight = $('#cycle_covers').find('a').height();

	$('#fitzWrapper').children().css('height', imgHeight);

}

resizeLoad();

$(window).resize(function(){
	resizeHome();
});*/
/**END OLD CAROUSEL**/


/**NEW CAROUSEL**/
// $('#cover_carousel').slick({
//   dots: true,
//   infinite: false,
//   speed: 900,
//   slidesToShow: 4,
//   slidesToScroll: 1,
//   slidesToScroll: 1,
//   autoplay: true,
//   autoplaySpeed: 2000,
//   dots: true,
//   responsive: [
//     {
//       breakpoint: 1024,
//       settings: {
//         slidesToShow: 3,
//         slidesToScroll: 3,
//         infinite: true,
//         dots: true
//       }
//     },
//     {
//       breakpoint: 600,
//       settings: {
//         slidesToShow: 2,
//         slidesToScroll: 2
//       }
//     },
//     {
//       breakpoint: 480,
//       settings: {
//         slidesToShow: 1,
//         slidesToScroll: 1
//       }
//     }
//   ]
// });
/**END NEW CAROUSEL**/

/**end homepage carousel re-size**/


	$('#preview').on('click', function(){

		if($(this).hasClass('active') !== true) {
			$(this).text('Close preview').addClass('active');


			var infoHeight = 100;

			$('.info').each(function(){

				infoHeight = infoHeight +  $(this).outerHeight();

			});


			$('.info').hide();

			$('.nano').css('height', infoHeight).fadeIn(300);
			$(".nano").nanoScroller(); //preview custom scroller
			$(".nano").nanoScroller({ flash: true });
		} else {
			$(this).text('Read preview').removeClass('active');

			$('.info').show();

			$('.nano').fadeOut(300);
			$(".nano").nanoScroller({ destroy: true });
		}
	});

});

function isEuropean(country) {
	var countries = ["AL", "AD", "AT", "BY", "BE", "BA", "BG", "HR", "CY", "CZ", "DK", "EE", "FO", "FI", "FR",
	     "DE", "GI", "GR", "HU", "IS", "IE", "IT", "LV", "LI", "LT", "LU", "MK", "MT", "MD", "MC", "NL", "NO",
			 "PL", "PT", "RO", "RU", "SM", "RS", "SK", "SI", "ES", "SE", "CH", "UA", "VA", "RS", "IM", "ME"];
  return countries.indexOf(country) > -1;
}

/* Payment */
$(document).ready( function(){

	function stripeResponseHandler(status, response) {
		var form = $('form#purchase');

		if (response.error) {
			$('.flash_error').remove();
			$($('#tab_4 h3')[0]).after('<div class="flash_error">' + response.error.message + '</div>');
			form.find('input[type=submit]').prop('disabled', false);
		}

		else {
			var token = response.id;
			form.append($('<input type="hidden" name="payment_stripe_token">').val(token));
			form.get(0).submit();
		}
	}

	$('form#purchase').submit(function(e){

		// Bypass Stripe token if using previous card
		if ($('#payment_card_existing').length) {
			if ($('#payment_card_existing').is(':checked')) {
				return true;
			}
 	    }

		var form = $(this);

		// Prevent re-submission
		form.find('input[type=submit]').prop('disabled', true);

		// Get the Stripe token
		Stripe.setPublishableKey('<%= STRIPE_PUBLIC_KEY %>');
		Stripe.card.createToken(form, stripeResponseHandler);

		// Stop immediate form submission
		return false;
	});

  if($('#successModal').length > 0) {
     $('#successModal').foundation('reveal', 'open');
  }

  //check month and year on purchase
  $('form#purchase').submit(function(e){

		var mm = $('#payment_card_expiration_month').val();
		var yy = $('#payment_card_expiration_year').val();
		var d = new Date();
		var y = d.getFullYear();


		if((mm <= 12) && (yy >= y)) {

			$('#payment_card_expiration_month').css('background', 'url(<%= asset_path "success.png" %>) 98% 50% no-repeat').removeClass('errorstate');
			$('#payment_card_expiration_year').css('background', 'url(<%= asset_path "success.png" %>) 98% 50% no-repeat').removeClass('errorstate');
			$(this).find('input[type=submit]').prop('disabled', false);

		} else if((mm > 12) && (yy >= y)) {

			$('#payment_card_expiration_month').css('background', 'url(<%= asset_path "fail.png" %>) 98% 50% no-repeat').addClass('errorstate');;
			$('#payment_card_expiration_year').css('background', 'url(<%= asset_path "success.png" %>) 98% 50% no-repeat').removeClass('errorstate');
			$(this).find('input[type=submit]').prop('disabled', true);

		} else if((mm <= 12) && (yy < y)) {

			$('#payment_card_expiration_month').css('background', 'url(<%= asset_path "success.png" %>) 98% 50% no-repeat').removeClass('errorstate');
			$('#payment_card_expiration_year').css('background', 'url(<%= asset_path "fail.png" %>) 98% 50% no-repeat').addClass('errorstate');
			$(this).find('input[type=submit]').prop('disabled', true);

		} else {

			$('#payment_card_expiration_month').css('background', 'url(<%= asset_path "fail.png" %>) 98% 50% no-repeat').addClass('errorstate');
			$('#payment_card_expiration_year').css('background', 'url(<%= asset_path "fail.png" %>) 98% 50% no-repeat').addClass('errorstate');
			$(this).find('input[type=submit]').prop('disabled', true);

		}

	});


});



/* Form navigation & validation*/
$(document).on('open', '[data-reveal]', function() {
	var coupon_discount = {};

		$('#coupon').blur(function(){
			var form_type = $('#subscription_fiction_types').length ? 'subscriptions' : 'publications';

			// Is it valid?
			$.ajax({
				type: 'POST',
				url: '/coupon/validate',
				data: { type: form_type, coupon: $(this).val() }
			}).done(function(discount){
				coupon_discount = discount;
			});
		});


		/**payment overlay tabs script**/

		//make none of the tab titles clickable
		$('#tabs_wrapper').find('.tab-links').on('click', 'a', function(){
			return false;
		});

		$('#next').on('click', function(e)  {
		    var currentAttrValue = $('#tabs_wrapper').find('.active').next().children().attr('href');
		    var AttrValue = $('#tabs_wrapper').find('.active').children().attr('href');
		    var attrText = $('#tabs_wrapper').find('.active').next().next().find('a').text();
			var nextTab = $('#tabs_wrapper').find('.active').next();

			switch(currentAttrValue) {
			    case '#tab_2':
			        $('#tabs_wrapper').find('#next').text('Enter ' + attrText).prepend('<span></span>');
			        break;
			    case '#tab_3':
			        $('#tabs_wrapper').find('#next').text('Enter ' + attrText).prepend('<span></span>');
			        break;
			    case '#tab_4':
			        $('#tabs_wrapper').find('#next').text('Buy Now').prepend('<span></span>');
			        break;
			}

			  if ($('#tabs_wrapper').find('.active').children().attr('href') !== '#tab_4') {
					$('#tabs_wrapper').find('#prev').removeClass('disabled'); //undisable nav buttons
					  // Show/Hide Tabs
				    $('#tabs_wrapper ' + currentAttrValue).show().siblings().hide();
					  // Change/remove current tab to active
					  $('#tabs_wrapper').find('.active').removeClass('active').next().addClass('active');

				  if(AttrValue == '#tab_3'){
				  	 $('#tabs_wrapper').find('#next').addClass('disabled');
				  }

			  }

			  e.preventDefault();
	    });

		$('#prev').on('click', function(e)  {
				var currentAttrValue = $('#tabs_wrapper').find('.active').prev().children().attr('href');
		        var AttrValue = $('#tabs_wrapper').find('.active').children().attr('href');
		        var attrText = $('#tabs_wrapper').find('.active').find('a').text();
				var nextTab = $('#tabs_wrapper').find('.active').prev();

				switch(currentAttrValue) {
					case '#tab_1':
				        $('#tabs_wrapper').find('#next').text('Choose Products').prepend('<span></span>');
				        break;
				    case '#tab_2':
				        $('#tabs_wrapper').find('#next').text('Enter ' + attrText).prepend('<span></span>');
				        break;
				    case '#tab_3':
				        $('#tabs_wrapper').find('#next').text('Enter ' + attrText).prepend('<span></span>');
				        break;
				    case '#tab_4':
				        $('#tabs_wrapper').find('#next').text('Enter ' + attrText).prepend('<span></span>');
				        break;
				}

				if($('#tabs_wrapper').find('.active').children().attr('href') !== '#tab_1') {
						$('#tabs_wrapper').find('#next').removeClass('disabled'); //undisable nav buttons
					  // Show/Hide Tabs
						$('#tabs_wrapper ' + currentAttrValue).show().siblings().hide();
						// Change/remove current tab to active
						$('#tabs_wrapper').find('.active').removeClass('active').prev().addClass('active');


				  if(AttrValue == '#tab_2'){
				  	 $('#tabs_wrapper').find('#prev').addClass('disabled');
				  }

				}

				e.preventDefault();
		});

		/**end payment overlay tab script**/



    	$("#account_email").blur(function(){

			$el = $(this).val();

			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			if( !emailReg.test( $el ) ) {
				$(this).css('background', 'url(<%= asset_path "fail.png" %>) 98% 50% no-repeat').addClass('errorstate');
				return false;
			} else if($el == ''){
				$(this).css('background', 'url(<%= asset_path "fail.png" %>) 98% 50% no-repeat').addClass('errorstate');
				return false;
			} else {
				$(this).css('background', 'url(<%= asset_path "success.png" %>) 98% 50% no-repeat').removeClass('errorstate');
				return true;
			}
		});

		function checkInput(el, length, val){
			if(val.length < length){
					el.css('background', 'url(<%= asset_path "fail.png" %>) 98% 50% no-repeat').addClass('errorstate');
					return true;
				} else if(val == ''){
					el.css('background', 'url(<%= asset_path "fail.png" %>) 98% 50% no-repeat').addClass('errorstate');
					return false;
				} else {
					el.css('background', 'url(<%= asset_path "success.png" %>) 98% 50% no-repeat').removeClass('errorstate');
					return true;
				}
		}


		function checkInputDate(el, length, val){

			var months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];


			if(val.length < length || months.indexOf(val) < 0) {
				el.css('background', 'url(<%= asset_path "fail.png" %>) 98% 50% no-repeat').addClass('errorstate');
				return true;
			} else if (months.indexOf(val) > -1){
				el.css('background', 'url(<%= asset_path "success.png" %>) 98% 50% no-repeat').removeClass('errorstate');
				return true;
			}
		}


		function ValidateYear(obj, el)
		{
			var nudate = new Date();
		    var nuyear = nudate.getFullYear();

			if (obj.length !=0)
			{
				var text = /^(19|20)\d{2}$/;

				if ((obj != "") && (!text.test(obj)))
				{
					el.css('background', 'url(<%= asset_path "fail.png" %>) 98% 50% no-repeat').addClass('errorstate');
				}

				if (obj.length>4)
				{
					el.css('background', 'url(<%= asset_path "fail.png" %>) 98% 50% no-repeat').addClass('errorstate');
				} else if (text.test(obj) && (obj < nuyear)){
					el.css('background', 'url(<%= asset_path "fail.png" %>) 98% 50% no-repeat').addClass('errorstate');
				} else if (text.test(obj) && (obj >= nuyear)){
					el.css('background', 'url(<%= asset_path "success.png" %>) 98% 50% no-repeat').removeClass('errorstate');
				}
			}
		}
		if (typeof account_password !== 'undefined') {
				//password strength
				account_password.oninput = function(){
					var summin = $(this).val();
					checkInput($(this), 7, summin);
				}
		}
		if (typeof payment_card_number !== 'undefined') {
				//card number
				payment_card_number.oninput = function(){
					var summin = $(this).val();
					checkInput($(this), 15, summin);
				}
		}
		if (typeof payment_cvc !== 'undefined') {
				//card cvc
				payment_cvc.oninput = function(){
					var summin = $(this).val();
					checkInput($(this), 2, summin);
				}
		}
		if (typeof payment_card_expiration_month !== 'undefined') {
				//card cvc
				payment_card_expiration_month.oninput = function(){

					if (this.value.length > 2)
        				this.value = this.value.slice(0,2);

				}

				payment_card_expiration_month.oninput = function(){
					var summin = $(this).val();
					checkInputDate($(this), 1, summin);
				}
		}
		if (typeof payment_card_expiration_year !== 'undefined') {
				//card cvc
				payment_card_expiration_year.oninput = function(){

					if (this.value.length > 4)
        				this.value = this.value.slice(0,4);

				}

				payment_card_expiration_year.oninput = function(){
					var summin = $(this).val();
					ValidateYear(summin, $(this));
				}
		}
		if (typeof account_password_confirmation !== 'undefined') {

				//password match confirmation
				account_password_confirmation.oninput = function(){

					var pw = $('#account_password').val();
					var pwc = $(this).val();

					if(pw == pwc) {
						$(this).css('background', 'url(<%= asset_path "success.png" %>) 98% 50% no-repeat').removeClass('errorstate');;
							return true;
					} else {
						$(this).css('background', 'url(<%= asset_path "fail.png" %>) 98% 50% no-repeat').addClass('errorstate');;
							return false;
					}

				}
		}
				//check that at least one checkbox has been checked when the user click onto the next box
				//next box click event not inluded
				//disable other checkbox when clicked
				$('input[type="checkbox"]').on('click', function(){
					$id = $(this).attr('id');

					$('input[type="checkbox"]').each(function(){
						if($('input[type="checkbox"]:checked').length > 0){
							if($(this).attr('id') !== $id) {
								$(this).attr('disabled', 'disabled');
							}
						} else {
							$(this).removeAttr('disabled');
						}
					});
				});

				$('#next').on('click', function(){

					if($('input[type="checkbox"]:checked').length > 0){

            var country = $('#customer_address_country option:selected').val();
						var price = Number($('input[type="checkbox"]:checked').prev().attr('data-price'));

						// shipping - products
						if ($('#subscription_fiction_types').length == 0) {
						  if (country == 'GB' || !$('#product_types_hardback').prop('checked')) {
							  // nothing to add
						  }
						  else if (isEuropean(country)) {
							  price += 3.50
						  }
						  else {
							  price += 6.00
						  }

						  // quantity
						  price *= $('#quantity').val();

						}
            // shipping - subscriptions
						else {
              if (country == 'GB') {
								// nothing to add
							}
							else if (isEuropean(country)) {
								price += 10.00
							}
							else {
								price += 20.00
						  }
						}

						if (coupon_discount.percent) {
							price *= ((100 - coupon_discount.percent) / 100);
						}

						if (coupon_discount.fixed) {
							price -= coupon_discount.fixed;
						}

						$('#total-price').text('Your card will be charged £' + price.toFixed(2))

					}
				});
});
