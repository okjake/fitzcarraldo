var keystone = require('keystone');
var Types = keystone.Field.Types;
var Prize = new keystone.List('Prize', {
	sortable: true,
    defaultSort: 'sortOrder',
    map: { name: 'name' },
    autokey: { path: 'key', from: 'name', unique: true },
    nocreate: false,
    nodelete: false
});

Prize.add({
    name: { type: String, initial: true },
	images: {
        detail: { type: Types.CloudinaryImage, autoCleanup : true }
    },
    content: { type: Types.Html, wysiwyg: true, height: 400 }
}, 'Prize teaser', {
    teaser: {
        about: { type: Types.Html, wysiwyg: false, height: 400 }
    }
});

Prize.register();
