var _ = require("lodash");

var CODEKEY = {
	Subscription: "appliesToSubscription",
	Bundle: "appliesToBundle",
	Book: "appliesTo",
	ToteBag: "appliesToToteBag"
};

function codeAppliesToProductType(code, productType, format) {
	if (!code || !productType)
		reject(
			new Error("Wrong number of arguments passed to codeAppliesToProductType")
		);

	// if code is for a specific item but current item isn't it, return false
	// if promotype isn't equal productType return false
	if (
		(code[CODEKEY[code.promoType]].length &&
			code[CODEKEY[code.promoType]].indexOf(productType.id) === -1) ||
		format !== code.promoType
	)
		return false;

	return true;
}

function returnFormat(item) {
	return item.format === "Subscription" ||
		item.format === "Bundle" ||
		item.format === "ToteBag"
		? item.format
		: "Book";
}

function codeApplies(code, item) {
	const format = returnFormat(item);
	return code.promoType !== "Full Order"
		? codeAppliesToProductType(code, item, format)
		: false;
}

function applyPromoToCustomizedBundles(appliedCode, order, totalBooks) {
	if (appliedCode.discount.percent) {
		var totalBeforeDiscount = order.sum.excludingShipping;
		var shipping = order.sum.total - order.sum.excludingShipping;

		var discountedExcludingShipping =
			order.sum.excludingShipping *
			((100 - appliedCode.discount.percent) / 100);

		order.excludingShippingDiscount =
			order.sum.excludingShipping - discountedExcludingShipping;

		order.sum.excludingShipping = discountedExcludingShipping;
		order.sum.total = discountedExcludingShipping + shipping;

		var strikeThroughDiscount = discountedExcludingShipping.toFixed(2);

		// stop checkout strikethroughs and make a seperate slot for discount amount
		order.checkoutMessage =
			totalBooks +
			" books for <s>£" +
			totalBeforeDiscount +
			"</s> £" +
			strikeThroughDiscount;

		return order.checkoutMessage;
	}
}

function applyDiscountPercentForProduct(
	bookCount,
	excludingShipping,
	result,
	appliedCode
) {
	if (!result || !appliedCode)
		reject(
			new Error(
				"Wrong number of arguments passed to applyDiscountPercentForProduct"
			)
		);
	// Comment out - this stops a code from working with less than 4 books
	//if (bookCount <= 3 && returnFormat(result) === "Book")
	//	return {
	//		sumExcludingShippingMinusDiscount: result.sum.excludingShipping,
	//		excludingShippingTotal: result.sum.excludingShipping
	//	};

	var discounted =
		result.sum.excludingShipping * ((100 - appliedCode.discount.percent) / 100);
	excludingShipping += discounted;
	var sumExcludingShippingMinusDiscount =
		result.sum.excludingShipping - discounted;

	return {
		sumExcludingShippingMinusDiscount: sumExcludingShippingMinusDiscount,
		excludingShippingTotal: excludingShipping
	};
}

function applyDiscountFixedForProduct(
	bookCount,
	excludingShipping,
	result,
	appliedCode
) {
	if (!result || !appliedCode)
		reject(
			new Error(
				"Wrong number of arguments passed to applyDiscountFixedForProduct"
			)
		);

	// Comment out - this stops a code from working with less than 4 books
	//if (bookCount <= 3 && returnFormat(result) === "Book")
	//	return {
	//		sumExcludingShippingMinusDiscount: result.sum.excludingShipping,
	//		excludingShippingTotal: result.sum.excludingShipping
	//	};

	var discounted = Math.max(
		result.sum.excludingShipping - appliedCode.discount.fixed * result.qty,
		0
	);
	excludingShipping += discounted;
	var sumExcludingShippingMinusDiscount =
		result.sum.excludingShipping - discounted;

	return {
		sumExcludingShippingMinusDiscount: sumExcludingShippingMinusDiscount,
		excludingShippingTotal: excludingShipping
	};
}

function applyDiscountPercentForOrder(order, appliedCode, results) {
	var shipping = order.sum.total - order.sum.excludingShipping;
	var discountedExcludingShipping =
		order.sum.excludingShipping * ((100 - appliedCode.discount.percent) / 100);

	for (var i = 0; i < results.length; i++) {
		var excludingShipping = 0;
		var discounted =
			results[i].sum.excludingShipping *
			((100 - appliedCode.discount.percent) / 100);
		results[i].discount = results[i].sum.excludingShipping - discounted;
	}

	order.excludingShippingDiscount =
		order.sum.excludingShipping - discountedExcludingShipping;
	order.sum.excludingShipping = discountedExcludingShipping;
	order.sum.total = discountedExcludingShipping + shipping;
	return order;
}

function applyDiscountFixedForOrder(order, appliedCode) {
	var discountedExcludingShipping =
		order.sum.excludingShipping - appliedCode.discount.fixed;
	order.excludingShippingDiscount = appliedCode.discount.fixed;
	order.sum.excludingShipping = discountedExcludingShipping;
	order.sum.total -= appliedCode.discount.fixed;
	if (order.sum.total < 0) order.sum.total = 0;
	if (order.sum.excludingShipping < 0) order.sum.excludingShipping = 0;
	return order;
}

function priceForHighestPriceBooksRemaining(
	discountBookNumber,
	results,
	appliedCode
) {
	if (!discountBookNumber) return 0;

	// get lowest to highest
	var res = _.sortBy(results, function(a) {
		return a["individual"].excludingShipping;
	});

	// apply promo to remainder item
	var bookPrices = _.reduce(
		res,
		function(acc, result) {
			if (returnFormat(result) !== "Book") return acc;
			for (var i = 0; i < parseInt(result.qty); i++) {
				var item = result["individual"].excludingShipping;

				if (
					appliedCode &&
					appliedCode.appliesTo.length &&
					(appliedCode.promoType === "Full Order" ||
						appliedCode.promoType === "Book")
				) {
					var individualExcludingShipping = appliedCode.discount.percent
						? item - (item / 100) * appliedCode.discount.percent
						: item - appliedCode.discount.fixed;
					acc.push(individualExcludingShipping);
				} else {
					acc.push(item);
				}
			}
			return acc;
		},
		[]
	);

	return _.sum(bookPrices.slice(discountBookNumber, bookPrices.length));
}

function totalOfProductsNotInPromo(results, order) {
	// NOTE - need to apply dicounts to these products
	var priceArray = _.reduce(
		results,
		function(acc, result) {
			if (returnFormat(result) === "Book") {
				var discount = result.discount || 0;
				acc.push(parseFloat(result.sum.excludingShipping - discount));
			}
			return acc;
		},
		[]
	);

	return order.sum.excludingShipping - _.sum(priceArray);
}

function returnDiscountObjectWithPromo(appliedCode, bundles) {
	if (
		!appliedCode ||
		appliedCode.promoType === "Subscription" ||
		appliedCode.promoType === "ToteBag"
	)
		return bundles;

	// if percent and fixed amount aren't filled out
	if (!appliedCode.discount.percent && !appliedCode.discount.fixed)
		return bundles;

	// if the code applies to specific books or bundles
	if (appliedCode.appliesTo.length || appliedCode.appliesToBundle.length)
		return bundles;

	var bundleWithPromo = _.chain(bundles)
		.values()
		.map(function(x) {
			if (appliedCode.discount.percent) {
				return x - (x / 100) * appliedCode.discount.percent;
			} else if (appliedCode.discount.fixed) {
				return x - appliedCode.discount.fixed;
			}
		})
		.reduce(function(acc, x, i) {
			acc[_.keys(bundles)[i]] = x;
			return acc;
		}, {})
		.value();

	return bundleWithPromo;
}

function applyDiscountForCustomBundle(bookCount, order, appliedCode, results) {
	var bundles = {
		4: 35,
		8: 70,
		12: 100
	};

	var discountPrice = returnDiscountObjectWithPromo(appliedCode, bundles);

	if (order.sum.excludingShipping <= discountPrice[bookCount]) return order;

	var discountObject = _.reduce(
		_.keys(discountPrice),
		function(acc, bookNumber) {
			var remainder = bookCount % parseInt(bookNumber);
			acc.discount =
				remainder >= 0 && remainder < 4 ? parseInt(bookNumber) : acc.discount;
			acc.remainder =
				remainder >= 0 && remainder < 4 ? remainder : acc.remainder;
			return acc;
		},
		{ discount: 0, remainder: 0 }
	);

	var nonBookTotal = totalOfProductsNotInPromo(results, order);

	order.sum.excludingShipping =
		parseFloat(discountPrice[discountObject.discount].toFixed(2)) +
		priceForHighestPriceBooksRemaining(
			discountObject.discount,
			results,
			appliedCode
		) +
		nonBookTotal;

	order.sum.total = order.sum.excludingShipping + order.sum.shipping;

	order.checkoutMessage = _.isEqual(discountPrice, bundles)
		? discountObject.discount +
		  " books for £" +
		  discountPrice[discountObject.discount]
		: discountObject.discount +
		  " books was for £" +
		  bundles[discountObject.discount] +
		  ", now £" +
		  +discountPrice[discountObject.discount];

	return order;
}

function codeAppliesToOrder(code) {
	return code.promoType === "Full Order";
}

module.exports = {
	applyDiscountFixedForProduct,
	applyDiscountPercentForProduct,
	applyDiscountFixedForOrder,
	applyDiscountPercentForOrder,
	applyPromoToCustomizedBundles,
	applyDiscountForCustomBundle,
	codeAppliesToOrder,
	codeApplies,
	returnFormat
};
