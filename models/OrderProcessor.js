var keystone = require("keystone");
var orderHelpers = require("./OrderProcessorHelpers/orderHelpers.js");
var _ = require("lodash");

function returnProductModel(keystone, item) {
	var productTypeModels = {
		subscription: keystone.lists.SubscriptionPlan.model,
		publication: keystone.lists.Publication.model,
		bundle: keystone.lists.Bundle.model,
		totebag: keystone.lists.ToteBag.model
	};

	if (!Object.prototype.hasOwnProperty.call(productTypeModels, item["type"]))
		return next(new Error("Invalid item type"));

	return productTypeModels[item["type"]];
}

module.exports.create = function(data, next) {
	var shipping = data.shipping,
		promoCode = data.promoCode,
		additional = data.additional,
		total = 0,
		stack = [];

	var order = {
		description: "",
		items: [],
		sum: {
			excludingShipping: 0,
			shipping: 0,
			total: 0
		}
	};

	// check promo codes

	keystone.lists.PromoCode.model
		.findOne({ code: { $regex: new RegExp("^" + promoCode + "$", "i") } })
		.exec(function(err, appliedCode) {
			// if the promocode exists, check the promoType for the product
			for (var i = 0; i < data.contents.length; i++) {
				var item = data.contents[i],
					list = null;

				list = returnProductModel(keystone, item);
				stack.push(price(list, item.id, shipping, item.qty, item.notes));
			}

			Promise.all(stack)
				.catch(next)
				.then(function(results) {
					// results - basket contents
					if (results.length == data.contents.length) {
						var bookCount = _.chain(results)
							.filter(x => orderHelpers.returnFormat(x) === "Book")
							.reduce((acc, x) => {
								acc = acc + parseInt(x.qty);
								return acc;
							}, 0)
							.value();

						var cumulativeBookCount = 0;

						// loop through contents
						for (var i = 0; i < results.length; i++) {
							var excludingShipping = 0;
							var discountObject = {};

							// NOTE - stopped book promos when custom bundles are applied
							if (
								appliedCode &&
								orderHelpers.codeApplies(appliedCode, results[i])
							) {
								discountObject = appliedCode.discount.percent
									? orderHelpers.applyDiscountPercentForProduct(
											bookCount,
											excludingShipping,
											results[i],
											appliedCode
									  )
									: orderHelpers.applyDiscountFixedForProduct(
											bookCount,
											excludingShipping,
											results[i],
											appliedCode
									  );

								excludingShipping = discountObject.excludingShippingTotal;
								results[i].discount =
									discountObject.sumExcludingShippingMinusDiscount;
							} else {
								// if not a promo, get the sum excluding shipping
								excludingShipping += results[i].sum.excludingShipping;
								if (data.contents[i].type === "publication")
									cumulativeBookCount += Number(data.contents[i].qty);
							}

							order.sum.excludingShipping += excludingShipping;
							order.sum.shipping += results[i].sum.shipping;
							order.sum.total += excludingShipping + results[i].sum.shipping;
							order.description += i != 0 ? ", " : "";
							order.description += results[i].title;
							order.description +=
								results[i].qty > 1 ? " (x" + results[i].qty + ")" : "";
							if (results[i].notes) order.description += " " + results[i].notes;
						}

						// if there is a code and it applies to the whole order
						if (appliedCode && orderHelpers.codeAppliesToOrder(appliedCode)) {
							order = appliedCode.discount.percent
								? orderHelpers.applyDiscountPercentForOrder(
										order,
										appliedCode,
										results
								  )
								: orderHelpers.applyDiscountFixedForOrder(order, appliedCode);
						}
						if (cumulativeBookCount > 3 && false) {
							order = orderHelpers.applyDiscountForCustomBundle(
								cumulativeBookCount,
								order,
								appliedCode,
								results
							);
						}

						order.sum.total = parseFloat(order.sum.total).toFixed(2);
						order.sum.excludingShipping = parseFloat(
							order.sum.excludingShipping
						).toFixed(2);
						order.sum.shipping = parseFloat(order.sum.shipping).toFixed(2);
						order.items = results;
						order.additional = additional;
						var valid = ["uk", "eu", "world"];
						if (valid.indexOf(shipping) == -1) {
							return reject(new Error("Invalid shipping value"));
						}
						order.shipping = shipping;
						next(null, order);
					}
				});
		});
};

function price(list, id, shipping, qty, notes) {
	return new Promise(function(resolve, reject) {
		var valid = ["uk", "eu", "world"];

		if (valid.indexOf(shipping) == -1) {
			return reject(new Error("Invalid shipping value"));
		}

		if (!qty) qty = 1;

		list
			.findOne({ _id: id })
			.populate("author")
			.exec(function(err, result) {
				if (!result) return reject(new Error("Not found"));
				if (err) return reject(err);

				if (
					result.prices &&
					result.prices[shipping] &&
					result.prices[shipping] !== 0
				) {
					var base = result.prices["uk"],
						format = result.format,
						author = "";

					if (result.author) author = result.author.displayName;

					if (list == keystone.lists.SubscriptionPlan.model) {
						format = "Subscription";
						author = "Various";
						url = "/subscribe";
					} else if (list == keystone.lists.Bundle.model) {
						format = "Bundle";
						if (result.author === undefined) {
							author = "Assorted Authors";
						}
						url = "/collections/" + result.key;
					} else if (list == keystone.lists.ToteBag.model) {
						format = "ToteBag";
						url = "/tote-bags/" + result.key;
					} else {
						url = "/books/" + result.key;
					}

					var item = {
						id: id,
						qty: qty,
						title: result.title,
						key: result.key,
						url: url,
						format: format,
						author: author,
						individual: {
							excludingShipping: base,
							shipping: result.prices[shipping] - base,
							combined: result.prices[shipping]
						},
						sum: {
							excludingShipping: base * qty,
							shipping: (result.prices[shipping] - base) * qty,
							combined: result.prices[shipping] * qty
						},
						notes: notes
					};

					return resolve(item);
				}

				return reject(new Error("Unknown error"));
			});
	});
}
