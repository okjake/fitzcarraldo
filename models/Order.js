var keystone = require('keystone');
var Types = keystone.Field.Types;
var Order = new keystone.List('Order', {
    map: { name: 'customer.email', unique: 'false' },
    track: { createdAt: true },
    autokey: { path: 'key', from: 'createdAt', unique: true },
    nocreate: true,
    defaultSort: '-createdAt'
});

Order.add({
    shipped: { type: Boolean },
    refunded: { type: Boolean },
    notes: { type: Types.Textarea },
    additional: { type: Types.Textarea },
    customer: {
        email: { type: String, noedit: true },
        name: { type: String, noedit: true },
        address: { type: Types.Textarea, noedit: true }
    },
    items: { type: String, noedit: true },
    paid: { type: String, noedit: true },
    stripeID: { type: String, noedit: true }
});

Order.defaultColumns = 'items, customer.name, customer.address, refunded, shipped, createdAt'
Order.register();
