var keystone = require('keystone');
var Types = keystone.Field.Types;
var pageDescriptions = new keystone.List('pageDescriptions', {
	map: { name: 'title' },
    autokey: { path: 'key', from: 'text', unique: true }
});

pageDescriptions.add({
	title: { type: String, initial: true, index: true },
    text: { type: Types.Html, wysiwyg: true, initial: true },
	location: { type: String, initial: true, index: true }
});

pageDescriptions.register();
