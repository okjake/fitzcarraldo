var keystone = require("keystone");
var Types = keystone.Field.Types;
var ToteBag = new keystone.List("ToteBag", {
	sortable: true,
	defaultSort: "sortOrder",
	map: { name: "title" },
	autokey: { path: "key", from: "title", unique: true }
});

ToteBag.add({
	title: { type: String, initial: true, index: true },
	images: {
		detail: { type: Types.CloudinaryImage, autoCleanup: true }
	},
	description: { type: Types.Html, wysiwyg: true, height: 400 },
	prices: {
		uk: { type: Types.Money, currency: "en-gb", format: "£0,0.00" },
		eu: { type: Types.Money, currency: "en-gb", format: "£0,0.00" },
		world: { type: Types.Money, currency: "en-gb", format: "£0,0.00" }
	}
});

ToteBag.register();
