var keystone = require('keystone');
var Types = keystone.Field.Types;
var File = new keystone.List('File', {
    map: { name: 'title' },
    autokey: { path: 'key', from: 'title', unique: true },
    nocreate: true,
    nodelete: true
});

File.add({
    title: { type: String, initial: true, index: true },
    source: { type: Types.S3File }
});

File.register();
