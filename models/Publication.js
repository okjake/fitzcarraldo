var keystone = require('keystone');
var Types = keystone.Field.Types;
var Publication = new keystone.List('Publication', {
    sortable: true,
    defaultSort: 'sortOrder',
    perPage: 200,
    map: { name: 'title' },
    autokey: { path: 'key', from: 'title', unique: true }
});

Publication.add({
    title: { type: String, initial: true, index: true },
    isbn: { type: String },
    featured: { type: Types.Boolean },
    author: { type: Types.Relationship, ref: 'Author', initial: true },
    category: { type: Types.Select, options: 'essays, fiction' },
    images: {
        gallery: { type: Types.CloudinaryImage, autoCleanup : true },
        detail: { type: Types.CloudinaryImage, autoCleanup : true }
    },
    details: { type: String },
    publicationDetails: { type: String },
    summary: { type: Types.Html, wysiwyg: true },
    description: { type: Types.Html, wysiwyg: true, height: 400 },
    preview: { type: Types.Html, wysiwyg: true, height: 400 },
    format: { type: Types.Select, options: 'hardback, flapped paperback, digital' },
    file: { type: Types.S3File, dependsOn: { format: 'digital' }},
    prices: {
        uk: { type: Types.Money, currency: 'en-gb', format: '£0,0.00' },
        eu: { type: Types.Money, currency: 'en-gb', format: '£0,0.00' },
        world: { type: Types.Money, currency: 'en-gb', format: '£0,0.00' }
    },
    outOfStock: { type: Types.Boolean , initial: false },
}, 'Collaborator', {
    collaborator: {
        description: { type: String },
        about: { type: Types.Html, wysiwyg: true, height: 400 }
    }
});

Publication.relationship({ path: 'bundles', ref: 'Bundle', refPath: 'publication'});

Publication.register();
