var keystone = require('keystone');
var Types = keystone.Field.Types;
var Page = new keystone.List('Page', {
    autokey: { path: 'key', from: 'name', unique: true },
    nocreate: false,
    nodelete: true
});

Page.add({
    name: { type: String, initial: true },
    content: { type: Types.Html, wysiwyg: true, height: 400 },
    additionalOne: { type: String },
    additionalTwo: { type: String },
    additionalThree: { type: String }
});

Page.register();
