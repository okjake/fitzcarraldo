var keystone = require("keystone");
var Types = keystone.Field.Types;
var PromoCode = new keystone.List("PromoCode", {
	map: { name: "code" }
});

PromoCode.add({
	code: { type: String },
	discount: {
		fixed: {
			type: Types.Money,
			currency: "en-gb",
			format: "£0,0.00",
			note: "Fixed rate discount, in pounds"
		},
		percent: { type: Number, note: "Percentage discount" }
	},
	promoType: {
		type: Types.Select,
		options: "Full Order,Book,Subscription,Bundle,ToteBag"
	},
	appliesTo: {
		type: Types.Relationship,
		ref: "Publication",
		many: true,
		dependsOn: { promoType: "Book" },
		note: "If the promo code should apply to specific books, specify them here"
	},
	appliesToSubscription: {
		type: Types.Relationship,
		ref: "SubscriptionPlan",
		many: true,
		dependsOn: { promoType: "Subscription" },
		note:
			"If the promo code should apply to specific subscriptions, specify them here"
	},
	appliesToBundle: {
		type: Types.Relationship,
		ref: "Bundle",
		many: true,
		dependsOn: { promoType: "Bundle" },
		note:
			"If the promo code should apply to specific bundles, specify them here"
	},
	appliesToToteBag: {
		type: Types.Relationship,
		ref: "ToteBag",
		many: true,
		dependsOn: { promoType: "ToteBag" },
		note:
			"If the promo code should apply to specific tote bags, specify them here"
	}
});

PromoCode.register();
