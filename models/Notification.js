var keystone = require("keystone");
var Types = keystone.Field.Types;
var Notification = new keystone.List("Notification", {
	map: { name: "title" },
	autokey: { path: "key", from: "text", unique: true }
});

Notification.add({
	title: { type: String, initial: true, index: true },
	activeNotification: { type: Types.Boolean },
	text: { type: Types.Html, wysiwyg: true, initial: true },
	location: {
		type: Types.Select,
		options:
			"home, about, basket, author, authors, distribution, foreign rights, privacy, prize, prize home, publication, shop, subscribe, bundles home, bundle, totebags home, totebag"
	}
});

Notification.register();
