var keystone = require('keystone');
var Types = keystone.Field.Types;
var Author = new keystone.List('Author', {
    autokey: { path: 'key', from: 'name', unique: true }
});

Author.add({
    name: { type: Types.Name, initial: true },
    displayName: { type: String },
    about: { type: Types.Html, wysiwyg: true, height: 400 }
});

Author.relationship({ path: 'publications', ref: 'Publication', refPath: 'author'});
Author.register();
