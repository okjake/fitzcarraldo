// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require("dotenv").load();

// Require keystone
var keystone = require("keystone");
var swig = require("swig");

const { constants } = require('crypto');

// Disable swig's bulit-in template caching, express handles it
swig.setDefaults({ cache: false });

keystone.init({
	name: "Fitzcarraldo Editions",
	brand: "Fitzcarraldo Editions",

	sass: "public",
	static: "public",
	favicon: "public/favicon.ico",
	views: "templates/views",
	"view engine": "swig",

	"custom engine": swig.renderFile,

	"auto update": true,
	session: true,
	auth: true,
	"user model": "User",
	"cookie secret":
		"K56bFaMA<3TTqQZOGZh>~_$CI)X>H<#]wL<Tto8W-Vn^^_WgfT[#_.c@XU^ReB9;",
	"https server options": { secureOptions: constants.SSL_OP_NO_TLSv1 | constants.SSL_OP_NO_TLSv1_1  },
	"ssl key": process.env.SSL_KEY,
	"ssl cert": process.env.SSL_CERT,
	"ssl ca": process.env.SSL_CA,
	"ssl port": process.env.SSL_PORT || 3001,
	"ssl host": process.env.SSL_HOST
});

if (process.env.NODE_ENV === "prod") {
	keystone.set("ssl", true);
	keystone.set("cloudinary secure", true);
}

keystone.import("models");

keystone.set("locals", {
	_: require("underscore"),
	env: keystone.get("env"),
	utils: keystone.utils,
	editable: keystone.content.editable
});

keystone.set("routes", require("./routes"));

keystone.set("nav", {
	"content management": [
		"authors",
		"files",
		"publications",
		"SubscriptionPlan",
		"bundles",
		"ToteBag",
		"pages",
		"prizes",
		"pageDescriptions"
	],
	administrators: "users"
});

keystone.start();
