var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};

    view.on('init', function(next) {

        keystone
            .list('Page')
            .model
            .findOne({
                _id: '560bf533a7049123be402a6d'
            })
            .exec(function(err, result) {
                locals.data.page = result;
                next(err);
            });
    });

    view.render('about');

};
