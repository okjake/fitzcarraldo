var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};

	view.on('init', function(next) {
		keystone
			.list('Notification')
			.model
			.findOne({
				location: 'author',
				activeNotification: true
			})
			.exec(function(err, result){
				locals.data.notification = result;
				next(err);
			});
	});

    view.on('init', function(next) {

        keystone
            .list('Author')
            .model
            .findOne({
                key: req.params.name
            })
            .exec(function(err, result) {
                if(result===null) {
                    req.flash('info', 'Sorry, we couldn\'t find a matching author');
                    return res.redirect('/authors')
                }
                result.populateRelated('publications', function(err) {
                    locals.data.author = result;
                    next(err);
                });
            });
    });

    view.render('author');

};
