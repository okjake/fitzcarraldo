var keystone = require("keystone");

exports = module.exports = function(req, res) {
    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};

	view.on('init', function(next) {
		keystone
			.list('Notification')
			.model
			.findOne({
				location: 'bundle',
				activeNotification: true
			})
			.exec(function(err, result){
				locals.data.notification = result;
				next(err);
			});
	});

    view.on("init", function(next) {
        keystone
            .list("Bundle")
            .model.findOne({
                key: req.params.bundle
            })
            .populate("publications")
            .populate("author")
            .exec(function(err, result) {
                locals.data.bundle = result;
                next(err);
            });
    });

    view.render("bundle");
};
