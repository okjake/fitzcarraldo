var keystone = require("keystone");

exports = module.exports = function(req, res) {
    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};

	view.on('init', function(next) {
		keystone
			.list('Notification')
			.model
			.findOne({
				location: 'publication',
				activeNotification: true
			})
			.exec(function(err, result){
				locals.data.notification = result;
				next(err);
			});
	});


    view.on("init", function(next) {
        keystone
            .list("Publication")
            .model.findOne({
                key: req.params.publication
            })
            .populate("author")
            .exec(function(err, result) {
                if(result===null) {
                    req.flash('info', 'Sorry, we couldn\'t find a matching publication');
                    return res.notfound('Publication not found');
                    }
                locals.data.publication = result;
                next(err);
            });
    });

    view.render("publication");
};
