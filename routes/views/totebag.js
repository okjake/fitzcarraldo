var keystone = require("keystone");

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;
	locals.data = {};

	view.on("init", function(next) {
		keystone
			.list("Notification")
			.model.findOne({
				location: "totebag",
				activeNotification: true
			})
			.exec(function(err, result) {
				locals.data.notification = result;
				next(err);
			});
	});

	view.on("init", function(next) {
		keystone
			.list("ToteBag")
			.model.findOne({
				key: req.params.totebag
			})
			.exec(function(err, result) {
				if(result===null) {
					req.flash('info', 'Sorry, we couldn\'t find a matching item');
					return res.notfound('Page not found');
				}
				locals.data.totebag = result;
				next(err);
			});
	});

	view.render("totebag");
};
