var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};

	view.on('init', function(next) {
		keystone
			.list('Notification')
			.model
			.findOne({
				location: 'shop',
				activeNotification: true
			})
			.exec(function(err, result){
				locals.data.notification = result;
				next(err);
			});
	});

	view.on('init', function(next) {
	    keystone
	        .list('pageDescriptions')
	        .model
			.findOne({
	            location: 'shop'
	        })
	        .exec(function(err, result){
	            locals.data.pageDescriptions = result;
	            next(err);
	        });
	});

    view.on('init', function(next) {
        var query = {};

        if (req.params.category) {
            query = { category: req.params.category }
            locals.category = req.params.category;
        }

        keystone
            .list('Publication')
            .model
            .find(query)
            .sort('sortOrder')
            .populate('author')
            .exec(function(err, results) {
                if(!results || results.length==0) {
                    req.flash('info', 'Sorry, we couldn\'t find matching publications');
                    return res.notfound('No publications found');
                }
                locals.data.publications = results;
                next(err);
            });
    });

    view.render('publications');

};
