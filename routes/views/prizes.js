var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};

	view.on('init', function(next) {
		keystone
			.list('Notification')
			.model
			.findOne({
				location: 'prize home',
				activeNotification: true
			})
			.exec(function(err, result){
				locals.data.notification = result;
				next(err);
			});
	});

    view.on('init', function(next) {

        keystone
            .list('Prize')
            .model
			.find()
            .exec(function(err, result) {
                locals.data.prizes = result;
                next(err);
            });

    });

    view.render('prizehome');

};
