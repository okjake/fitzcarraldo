var keystone = require('keystone');
exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};

	view.on('init', function(next) {
		keystone
			.list('Notification')
			.model
			.findOne({
				location: 'authors',
				activeNotification: true
			})
			.exec(function(err, result){
				locals.data.notification = result;
				next(err);
			});
	});

    view.on('init', function(next) {

        keystone
            .list('Author')
            .model
            .find()
            .sort('displayName')
            .exec(function(err, results) {
                keystone.populateRelated(results, 'publications', function(err) {
                    locals.data.alpha = {
                        'A': [], 'B': [], 'C': [], 'D': [], 'E': [], 'F': [], 'G': [],
                        'H': [], 'I': [], 'J': [], 'K': [], 'L': [], 'M': [], 'N': [],
                        'O': [], 'P': [], 'Q': [], 'R': [], 'S': [], 'T': [], 'U': [],
                        'V': [], 'W': [], 'X': [], 'Y': [], 'Z': [],
                    };

                    for (var i = 0; i < results.length; i++) {
                        var index = results[i].name.last.charAt(0).normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase();
                        if (locals.data.alpha[index] !== 'undefined') {
                            locals.data.alpha[index].push(results[i]);
                        }
                    }

					for (var i = 0; i < results.length; i++) {
						var index = results[i].name.last.charAt(0).toUpperCase();

						if(typeof locals.data.alpha[index] !== 'undefined' && locals.data.alpha[index] !== null) {
							var _item = locals.data.alpha[index];

							_item.sort(function(x, y){
								var nameA = x.name.last.toUpperCase();
								var nameB = y.name.last.toUpperCase();
								if (nameA < nameB) {
									return -1;
								}
								if (nameA > nameB) {
									return 1;
								}

								return 0;
							});
						}
					}

                    next(err);
                });
            });
    });

    view.render('authors');

};
