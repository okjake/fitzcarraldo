var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.section = 'home';
    locals.data = {
        publications: {}
    };

	view.on('init', function(next) {
		keystone
			.list('Notification')
			.model
			.findOne({
				location: 'home',
				activeNotification: true
			})
			.exec(function(err, result){
				locals.data.notification = result;
				next(err);
			});
	});

    view.on('init', function(next) {
        keystone
            .list('Publication')
            .model
            .findOne({ featured: true })
            .populate('author')
            .exec(function(err, result){
                locals.data.publications.featured = result;
                next(err);
            });
    });

    view.on('init', function(next) {
        keystone
            .list('Publication')
            .model
            .find({ category: 'fiction', featured: false })
            .sort('sortOrder')
            .populate('author')
            .limit(2)
            .exec(function(err, results){
                locals.data.publications.fiction = results;
                next(err);
            });
    });

    view.on('init', function(next) {
        keystone
            .list('Publication')
            .model
            .find({ category: 'essays', featured: false })
            .sort('sortOrder')
            .populate('author')
            .limit(2)
            .exec(function(err, results){
                locals.data.publications.essays = results;
                next(err);
            });
    });

    view.render('index');

};
