var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};

	view.on('init', function(next) {
		keystone
			.list('Notification')
			.model
			.findOne({
				location: 'distribution',
				activeNotification: true
			})
			.exec(function(err, result){
				locals.data.notification = result;
				next(err);
			});
	});


    view.on('init', function(next) {

        keystone
            .list('Page')
            .model
            .findOne({
                _id: '560bf567a7049123be402a6e'
            })
            .exec(function(err, result) {
                locals.data.page = result;
                next(err);
            });
    });

    view.render('distribution');

};
