var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};

	view.on('init', function(next) {
		keystone
			.list('Notification')
			.model
			.findOne({
				location: 'prize',
				activeNotification: true
			})
			.exec(function(err, result){
				locals.data.notification = result;
				next(err);
			});
	});


    view.on('init', function(next) {

        keystone
			.list('Prize')
			.model
			.findOne({
				key: req.params.prize
			})
            .exec(function(err, result) {
                locals.data.prize = result;
                next(err);
            });

    });

    view.render('prizepage');

};
