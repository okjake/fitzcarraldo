var keystone = require("keystone");

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;
	locals.data = {};

	view.on("init", function(next) {
		keystone
			.list("Notification")
			.model.findOne({
				location: "totebags home",
				activeNotification: true
			})
			.exec(function(err, result) {
				locals.data.notification = result;
				next(err);
			});
	});

	view.on("init", function(next) {
		keystone
			.list("pageDescriptions")
			.model.findOne({
				location: "totebags"
			})
			.exec(function(err, result) {
				locals.data.pageDescriptions = result;
				next(err);
			});
	});

	view.on("init", function(next) {
		keystone
			.list("ToteBag")
			.model.find()
			.exec(function(err, results) {
				locals.data.totebags = results;
				next(err);
			});
	});

	view.render("totebags");
};
