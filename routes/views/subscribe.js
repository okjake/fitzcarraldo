var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};


	view.on('init', function(next) {
		keystone
			.list('Notification')
			.model
			.findOne({
				location: 'subscribe',
				activeNotification: true
			})
			.exec(function(err, result){
				locals.data.notification = result;
				next(err);
			});
	});


    view.on('init', function(next) {

        keystone
            .list('Page')
            .model
            .findOne({
                _id: '560bf804bc153025c051a12e'
            })
            .exec(function(err, result) {
                locals.data.page = result;
                next(err);
            });
    });

    view.on('init', function(next) {

        keystone
            .list('SubscriptionPlan')
            .model
            .find()
            .sort('sortOrder')
            .exec(function(err, plans) {
                locals.data.subscriptionPlans = plans;
                next(err);
            });
    });

    view.render('subscribe');

};
