var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    view.render('privacy');

	view.on('init', function(next) {
		keystone
			.list('Notification')
			.model
			.findOne({
				location: 'privacy',
				activeNotification: true
			})
			.exec(function(err, result){
				locals.data.notification = result;
				next(err);
			});
	});


};
