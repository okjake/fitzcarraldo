var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};

    view.on('init', function(next) {
        locals.data.stripePublic = process.env.STRIPE_PUBLIC;
        next();
    });

	view.on('init', function(next) {
		keystone
			.list('Notification')
			.model
			.findOne({
				location: 'basket',
				activeNotification: true
			})
			.exec(function(err, result){
				locals.data.notification = result;
				next(err);
			});
	});

    view.render('basket');

};
