var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.data = {};

	view.on('init', function(next) {
		keystone
			.list('Notification')
			.model
			.findOne({
				location: 'foreign rights',
				activeNotification: true
			})
			.exec(function(err, result){
				locals.data.notification = result;
				next(err);
			});
	});

    view.on('init', function(next) {

        keystone
            .list('Page')
            .model
            .findOne({
                key: 'foreign-rights'
            })
            .exec(function(err, result) {
                locals.data.page = result;
                next(err);
            });
    });

    view.render('foreignrights');

};
