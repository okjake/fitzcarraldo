var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('routes', middleware.initErrorHandlers);

//Handle 404 errors
keystone.set('404', function (req, res, next) {
	res.notfound();
});

// Handle other errors
keystone.set('500', function (err, req, res, next) {
	var title, message;
	if (err instanceof Error) {
		message = err.message;
		err = err.stack;
	}
	res.status(500).render('errors/500', {
		err: err,
		errorTitle: title,
		errorMsg: message
	});
});

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
	api: importRoutes('./api')
};

// Setup Route Bindings
exports = module.exports = function(app) {
	// Views
	app.all('*', middleware.removeWWW);
	app.all('*', middleware.forceSSL);
	app.get('/', routes.views.index);
	app.get('/books/:publication?', routes.views.publication);
	app.get('/publications/:publication?', routes.views.publication);
	app.get('/shop', routes.views.publications);
	app.get('/shop/:category', routes.views.publications);
	app.get('/authors', routes.views.authors);
	app.get('/authors/:name', routes.views.author);
	app.get('/privacy', routes.views.privacy);
	app.get('/subscribe', routes.views.subscribe);
	app.get('/about', routes.views.about);
	app.get('/distribution', routes.views.distribution);
	app.get('/foreign-rights', routes.views.foreignrights);
	app.get('/basket', routes.views.basket);
	app.get('/prizes', routes.views.prizes);
	app.get('/prizes/:prize?', routes.views.prize);
	app.get('/collections', routes.views.bundles);
	app.get('/collections/:bundle?', routes.views.bundle);
	app.get('/tote-bags', routes.views.totebags);
	app.get('/tote-bags/:totebag?', routes.views.totebag);

	// API
	app.post('/api/basket', keystone.middleware.api, routes.api.basket);
	app.post('/api/checkout', keystone.middleware.api, routes.api.checkout);
	app.get(
		'/api/publications',
		keystone.middleware.api,
		routes.api.publications
	);
};
