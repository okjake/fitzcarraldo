var _ = require("underscore"),
	keystone = require("keystone");

exports.removeWWW = function(req, res, next) {
	if (
		req.headers &&
		req.headers.host &&
		req.headers.host.slice(0, 4) === "www."
	) {
		var newHost = req.headers.host.slice(4);
		return res.redirect(301, req.protocol + "://" + newHost + req.originalUrl);
	}

	next();
};

exports.forceSSL = function(req, res, next) {
	if (!req.secure && keystone.get("ssl") == true) {
		var sslHost =
				keystone.get("ssl host") ||
				keystone.get("host") ||
				process.env.HOST ||
				process.env.IP,
			sslPort = keystone.get("ssl port");

		if (!sslHost) {
			var gethost = req
				.get("host")
				.replace("http://", "")
				.split(":");
			sslHost = gethost[0];
		}

		if (sslPort) sslPort = ":" + sslPort;
		if (!req.secure) {
			return res.redirect("https://" + sslHost + sslPort + req.url);
		}
	}

	next();
};

/**
    Initialises the standard view locals
*/

exports.initLocals = function(req, res, next) {
	var locals = res.locals;

	locals.navLinks = [{ label: "Home", key: "home", href: "/" }];

	locals.user = req.user;
	keystone
		.list("File")
		.model.findOne({
			_id: "55e6f44c1ca7241f726cd032"
		})
		.exec(function(err, result) {
			locals.catalogue = result;
			next();
		});
};

/**
    Fetches and clears the flashMessages before a view is rendered
*/

exports.flashMessages = function(req, res, next) {
	var flashMessages = {
		info: req.flash("info"),
		success: req.flash("success"),
		warning: req.flash("warning"),
		error: req.flash("error")
	};

	res.locals.messages = _.any(flashMessages, function(msgs) {
		return msgs.length;
	})
		? flashMessages
		: false;

	next();
};

exports.initErrorHandlers = function(req, res, next) {
	res.err = function(err, title, message) {
		res.status(500).render('errors/500', {
			err: err,
			errorTitle: title,
			errorMsg: message
		});
	}
	res.notfound = function(title, message) {
		title = (title || "Page not found")
		message = (message || "Sorry, the page you requested can't be found.");
		res.status(404).render('errors/404', {
			title: title,
			errorMsg: message
		});
	}
	next();
};

/**
    Prevents people from accessing protected pages when they're not signed in
 */

exports.requireUser = function(req, res, next) {
	if (!req.user) {
		req.flash("error", "Please sign in to access this page.");
		res.redirect("/keystone/signin");
	} else {
		next();
	}
};
