require("dotenv").load();

var keystone = require("keystone"),
	moment = require("moment"),
	stripe = require("stripe")(process.env.STRIPE_SECRET),
	Order = require("../../models/OrderProcessor.js"),
	OrderRecord = keystone.list("Order").model;

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res),
		locals = res.locals;

	locals.data = {};

	view.on("init", function(next) {
		var data = JSON.parse(req.body.order);
		Order.create(data, function(err, order) {
			var charge = stripe.charges.create(
				{
					amount: (order.sum.total * 100).toFixed(0),
					currency: "gbp",
					source: req.body.token.id,
					receipt_email: req.body.token.email,
					description: order.description,
					metadata: {
						email: req.body.token.email,
						additional: order.additional
					}
				},
				function(err, charge) {
					if (err) return next(err);
					locals.data.charge = charge;

					var address = "";

					address += charge.source.address_line1
						? charge.source.address_line1
						: "";
					address += charge.source.address_line2
						? ", " + charge.source.address_line2
						: "";
					address += charge.source.address_state
						? ", " + charge.source.address_state
						: "";
					(address += charge.source.address_city
						? ", " + charge.source.address_city
						: ""),
						(address += charge.source.address_zip
							? ", " + charge.source.address_zip
							: "");
					address += charge.source.address_country
						? ", " + charge.source.address_country
						: "";

					var record = new OrderRecord({
						customer: {
							email: charge.metadata.email,
							name: charge.source.name,
							address: address
						},
						items: order.description,
						paid: order.amount,
						stripeID: charge.id
					});

					record.getUpdateHandler(req).process({}, function(err) {
						next(err);
					});
				}
			);
		});
	});

	view.render(function(err) {
		if (err) return res.apiError("error", err);
		res.apiResponse(locals.data);
	});
};
