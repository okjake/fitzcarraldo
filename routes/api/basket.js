var keystone = require("keystone"),
	Order = require("../../models/OrderProcessor.js");

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res),
		locals = res.locals;

	locals.data = {};

	view.on("init", function(next) {
		Order.create(req.body, function(err, order) {
			if (err) return next(err);
			locals.data.items = order.items;
			locals.data.totals = order.sum;
			locals.data.excludingShippingDiscount = order.excludingShippingDiscount;
			locals.data.description = order.description;
			locals.data.checkoutMessage = order.checkoutMessage;
			next(err);
		});
	});

	view.render(function(err) {
		if (err) return res.apiError("error", err);
		res.apiResponse(locals.data);
	});
};
