var keystone = require('keystone');

exports = module.exports = function(req, res) {

    var view = new keystone.View(req, res),
        locals = res.locals;

    locals.data = {};

    view.on('init', function(next) {
	var predicate = {};

	if (req.query && req.query.type && req.query.type !== 'all') {
            predicate = { category: req.query.type };
	}

        keystone.list('Publication').model.find(predicate).sort('sortOrder').exec(function(err, publications) {
	    if (err) return next(err);
            locals.data.publications = publications;
	    next();
	});
    });

    view.render(function(err) {
        if (err) return res.apiError('error', err);
        res.apiResponse(locals.data);
    });
}
